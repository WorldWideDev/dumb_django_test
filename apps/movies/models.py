# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Actor(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

class Movie(models.Model):
    title = models.CharField(max_length=100)

    # this_movie.cast.add(some_actor)
    # this_movie.cast.remove(some_actor)

class Cast(models.Model):
    film = models.ForeignKey(Movie)
    actor = models.ForeignKey(Actor)
    role = models.CharField(max_length=100)