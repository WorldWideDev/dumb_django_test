from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^actor/create', views.create_actor, name='create_actor'),
    url(r'^movie/create', views.create_movie, name='create_movie'),
    url(r'^actor/(?P<id>\d+)', views.show_actor, name='actor'),
    url(r'^movie/(?P<id>\d+)/$', views.show_movie, name='movie'),
    # url(r'^movie/append_cast/(?P<id>\d+)/$', views.append_cast, name='append_cast'),
]