# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Actor, Movie, Cast
from django.shortcuts import render, redirect

# Create your views here.
def index(req):
    print 'in index movies'
    context = {
        'actors': Actor.objects.all(),
        'movies': Movie.objects.all()
    }
    return render(req, "movies/index.html", context)

def create_actor(req):
    print 'in create actor'
    Actor.objects.create(
        first_name=req.POST['first_name'],
        last_name=req.POST['last_name']
    )
    return redirect("movies:index")

def create_movie(req):
    print 'in create movie'
    Movie.objects.create(title=req.POST['title'])
    return redirect('movies:index')

def show_actor(req, id):
    this_actor = Actor.objects.get(id=id)
    context = {
        "actor": this_actor,
    }
    print this_actor
    return render(req, "movies/actor.html", context)

def show_movie(req, id):
    this_movie = Movie.objects.get(id=id)
    
    context = {
        "movie": this_movie,
        "actors": Actor.objects.all()
    }
    print this_movie
    return render(req, "movies/movie.html", context)

    return redirect("movies:index")