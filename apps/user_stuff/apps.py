# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class UserStuffConfig(AppConfig):
    name = 'user_stuff'
