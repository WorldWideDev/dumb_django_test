# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import bcrypt
from django.db import models

class TheUserManager(models.Manager):
    def create_or_fail(self, client):
        errors = []
        is_valid = True
        # first_name, last_name
        if len(client['first_name']) < 4 or len(client['last_name']) < 4:
            errors.append("name values must be at least three characters")
            is_valid = False
        
        # username
        if len(self.filter(username=client['username'])) > 0:
            print 'user here'
            errors.append("username exists in system")
            is_valid = False
            
        # password
        if len(client['password']) < 9:
            errors.append("passwords must be at least 8 characters")
            
        # password confirm
        if client['password'] != client['password_confirm']:
            errors.append("passwords do not match")

        if is_valid:
            salt = bcrypt.gensalt()
            hashed_pw = bcrypt.hashpw(client['password'].encode(), salt)
            print salt
            print hashed_pw
            return { "is_valid" : is_valid, "obj": self.create(
                first_name = client['first_name'],
                last_name = client['last_name'],
                username = client['username'],
                password = client['password'],
                salt = salt
            )}
        return {'is_valid' : is_valid, 'obj' : errors}

# Create your models here.
class TheUser(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=255)
    salt = models.CharField(max_length=255)
    objects = TheUserManager()

    def __str__(self):
        return self.first_name + " " + self.last_name