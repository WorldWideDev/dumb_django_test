# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import TheUser
from django.shortcuts import render, redirect
from django.contrib import messages

TEMPLATE_NAMES = {
    'index': 'user_stuff/index.html'
}

# Create your views here.
def index(req):
    return redirect("movies:index")
    # print TheUser.objects.all()
    # return render(req, TEMPLATE_NAMES['index'])

def create(req):
    print 'in user create'
    if (req.POST):
        user_or_errors = TheUser.objects.create_or_fail(req.POST)
        if not user_or_errors['is_valid']:
            for error in user_or_errors['obj']:
                messages.error(req, error)
            return render(req, TEMPLATE_NAMES['index'])
    return redirect('user:index')
